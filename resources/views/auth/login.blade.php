<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App favicon -->
        <link rel="shortcut icon" href="itlabil/images/default/logo.png">

        <!-- App css -->
        <link href="itlabil/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/style.css" rel="stylesheet" type="text/css" />

        <script src="itlabil/admin/js/modernizr.min.js"></script>

    </head>


    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg" style="background: url('itlabil/images/default/bg-login.jpg');background-size: cover;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.html" class="text-success">
                                    <span><img src="itlabil/images/default/logo.png" alt="" height="150px"></span>
                                </a>
                            </h2>

                            <form class="" role="form" method="POST" action="{{ url('/login') }}">
                            {{csrf_field()}}
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">No Induk</label>
                                        <input id="no_induk" type="no_induk" class="form-control" name="no_induk" value="{{ old('no_induk') }}">
                                        @if ($errors->has('no_induk'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('no_induk') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <!-- <a href="page-recoverpw.html" class="text-muted pull-right"><small>Forgot your password?</small></a> -->
                                        <label for="password">Password</label>
                                        <input name="password" class="form-control" type="password" required="" id="password" placeholder="Enter your password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">

                                        <div class="checkbox checkbox-custom">
                                            <input id="remember" type="checkbox" checked="">
                                            <label for="remember">
                                                Remember me
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2018 © SMK Pelita Pesawaran</p>
            </div>

        </div>



        <!-- jQuery  -->
        <script src="itlabil/admin/js/jquery.min.js"></script>
        <script src="itlabil/admin/js/popper.min.js"></script>
        <script src="itlabil/admin/js/bootstrap.min.js"></script>
        <script src="itlabil/admin/js/metisMenu.min.js"></script>
        <script src="itlabil/admin/js/waves.js"></script>
        <script src="itlabil/admin/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="itlabil/admin/js/jquery.core.js"></script>
        <script src="itlabil/admin/js/jquery.app.js"></script>

    </body>
</html>