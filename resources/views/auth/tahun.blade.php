<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App favicon -->
        <link rel="shortcut icon" href="itlabil/admin/images/favicon.ico">

        <!-- App css -->
        <link href="itlabil/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="itlabil/admin/css/style.css" rel="stylesheet" type="text/css" />

        <script src="itlabil/admin/js/modernizr.min.js"></script>

    </head>


    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg" style="background: url('itlabil/images/default/bg-login.jpg');background-size: cover;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.html" class="text-success">
                                    <span><img src="itlabil/images/default/logo.png" alt="" height="150px"></span>
                                </a>
                            </h2>

                            {!! Form::open(['route' => 'tahun.store','class'=>'form-horizontal'])!!}
                                {{ csrf_field() }}
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Tahun Ajaran</label>
                                        {!! Form::select('tahun_ajaran',$ta ,null,array('class'=>'form-control has-feedback')) !!}
                                        <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <!-- <a href="page-recoverpw.html" class="text-muted pull-right"><small>Forgot your password?</small></a> -->
                                        <label for="semester">Semester</label>
                                        <select class="form-control" name="semester" id="semester">
                                            <option value="ganjil">Ganjil</option>
                                            <option value="genap">Genap</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>

                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2018 © SMK Pelita Pesawaran</p>
            </div>

        </div>



        <!-- jQuery  -->
        <script src="itlabil/admin/js/jquery.min.js"></script>
        <script src="itlabil/admin/js/popper.min.js"></script>
        <script src="itlabil/admin/js/bootstrap.min.js"></script>
        <script src="itlabil/admin/js/metisMenu.min.js"></script>
        <script src="itlabil/admin/js/waves.js"></script>
        <script src="itlabil/admin/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="itlabil/admin/js/jquery.core.js"></script>
        <script src="itlabil/admin/js/jquery.app.js"></script>

    </body>
</html>