<div class="slimscroll-menu" id="remove-scroll">

    <div class="topbar-left">
        <a href="{{ url('/') }}" class="logo">
            <img src="{{ url('itlabil/admin/images/logo.png') }}" alt="" height="35px">
        </a>
    </div>

    <div class="user-box">
        <div class="user-img">
            <img src="{{ url('itlabil/admin/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
        </div>
        <h5>Admin</h5>
        <p class="text-muted">Administrator</p>
    </div>

    <div id="sidebar-menu">

        <ul class="metismenu" id="side-menu">

            <li>
                <a href="{{ url('/') }}">
                    <i class="fi-air-play"></i> <span> Beranda </span>
                </a>
            </li>

            <li>
                <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Master Data </span> <span class="menu-arrow"></span></a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="apps-calendar.html">Calendar</a></li>
                    <li><a href="apps-tickets.html">Tickets</a></li>
                </ul>
            </li>

        </ul>

    </div>

    <div class="clearfix"></div>

</div>