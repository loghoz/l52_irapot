<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswas';
    protected $fillable = [
        'no_induk','nama','agama_id','status_id','nisn','jk','tempat_lahir','tanggal_lahir',
        'anak_ke','sekolah_asal','kelas_terima','tanggal_terima',
        'alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp','photo','hapus',
    ];
    
    //relasi one to one ke siswa wali
    public function siswa_wali()
    {
        return $this->belongsTo('App\SiswaWali','no_induk');
    }

    //relasi ke agama
    public function agama()
    {
        return $this->belongsTo('App\Agama', 'agama_id');
    }

    //relasi ke status
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    //relasi ke agama
    public function pemkel()
    {
        return $this->hasMany('App\PembagianKelas');
    }
}
