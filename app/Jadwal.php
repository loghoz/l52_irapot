<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwals';
    
    protected $fillable = [
        'guru_id','hari_id','mata_pelajaran_id','kode_kelas','jam_ajar','jam_mulai','jam_selesai','tahun_ajaran_id',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }
    

    //relasi ke hari
    public function hari()
    {
        return $this->belongsTo('App\Hari', 'hari_id');
    }

    //relasi ke guru
    public function mapel()
    {
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    //relasi ke tahun ajaran
    public function ta()
    {
        return $this->belongsTo('App\TahunAjaran', 'tahun_ajaran_id');
    }
}
