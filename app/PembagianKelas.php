<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembagianKelas extends Model
{
    protected $table = 'pembagian_kelas';
    protected $fillable = [
        'siswa_id','kelas_id','tahun_ajaran_id',
    ];

    //relasi ke siswa
    public function siswa()
    {
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    //relasi ke kelas
    public function kelas()
    {
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    //relasi ke ta
    public function ta()
    {
        return $this->belongsTo('App\TahunAjaran', 'tahun_ajaran_id');
    }
}
