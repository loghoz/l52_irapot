<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = Auth::user()->role;

        if ($role=='guru') {
            return redirect()->route('guru.beranda.index');
        } else if ($role=='siswa') {
            return redirect()->route('siswa.beranda.index');
        } else {
            return redirect()->route('admin.beranda.index');
        }
    }
}
