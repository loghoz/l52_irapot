<?php

Route::auth();

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::resource('tahun', 'Web\Admin\TahunAjaranController');
Route::resource('admin/beranda', 'Web\Admin\BerandaController');

// jurusan
Route::resource('admin/jurusan', 'Web\Admin\JurusanController');
Route::resource('admin/kelas', 'Web\Admin\KelasController');
