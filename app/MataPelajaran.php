<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table = 'mata_pelajarans';
    protected $fillable = [
        'kode_mat_pel','mata_pelajaran','deskripsi',
    ];

    //relasi one to many ke jadwal
    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }

    //relasi one to many ke pelapel
    public function pemapel()
    {
        return $this->hasMany('App\PembagianMapel');
    }
}
