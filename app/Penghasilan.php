<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penghasilan extends Model
{
    protected $table = 'penghasilans';
    protected $fillable = [
        'penghasilan',
    ];
}
