<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembagianMapel extends Model
{
    protected $table = 'pembagian_mapels';
    protected $fillable = [
        'jurusan_id','mata_pelajaran_id','tahun_ajaran_id','guru_id',
    ];

    //relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }
    
    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    //relasi ke matpel
    public function matpel()
    {
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }

    //relasi ke ta
    public function ta()
    {
        return $this->belongsTo('App\TahunAjaran', 'tahun_ajaran_id');
    }
}
