<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaWali extends Model
{
    protected $table = 'siswa_walis';
    protected $primaryKey = 'no_induk';

    protected $fillable = [
        'no_induk','ayah_nama','ayah_tl','ayah_penghasilan','ayah_pendidikan','ayah_pekerjaan','ayah_telp',
        'ibu_nama','ibu_tl','ibu_penghasilan','ibu_pendidikan','ibu_pekerjaan','ibu_telp','ortu_alamat',
        'wali_nama','wali_tl','wali_penghasilan','wali_pendidikan','wali_pekerjaan','wali_telp','wali_alamat'
    ];

    //relasi ke siswa
    public function siswa()
    {
        return $this->hasOne('App\Siswa', 'no_induk');
    }
}
