<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table = 'tahun_ajarans';
    
    protected $fillable = [
        'tahun_ajaran',
    ];

    // relasi one to many ke pemkel
    public function pemkel()
    {
        return $this->hasMany('App\PembagianKelas');
    }

    // relasi one to many ke pemkel
    public function pemapel()
    {
        return $this->hasMany('App\PembagianMapel');
    }
    
    // relasi one to many ke pemkel
    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }
}
