<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusans';

    protected $fillable = [
        'kode_jurusan','jurusan','bidang_keahlian',
    ];

    // relasi one to many ke kelas
    public function kelas()
    {
        return $this->hasMany('App\Kelas');
    }

    // relasi one to many ke mata pelajaran
    public function pemapel()
    {
        return $this->hasMany('App\PembagianMapel');
    }
}
