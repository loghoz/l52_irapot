<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'gurus';

    protected $fillable = [
        'no_induk','agama_id','jabatan_id','nama','jk','tempat_lahir','tanggal_lahir',
        'alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos',
        'telp','photo','hapus',
    ];

    //relasi ke jabatan
    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'jabatan_id');
    }

    //relasi ke agama
    public function agama()
    {
        return $this->belongsTo('App\Agama', 'agama_id');
    }
}
