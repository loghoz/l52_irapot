<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $with = ['jurusan'];
    protected $fillable = [
        'jurusan_id','kode_kelas','tingkat',
    ];

    // relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }

    // relasi ke jurusan
    public function pemkel()
    {
        return $this->hasMany('App\PembagianKelas');
    }
}
