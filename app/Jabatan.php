<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatans';

    protected $fillable = [
        'jabatan',
    ];

    // relasi one to many ke guru
    public function guru()
    {
        return $this->hasMany('App\Guru');
    }
}
