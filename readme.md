
## About Semkapita

Sebuah Aplikasi Data Center SMK Pelita.

## Concept

Sebuah website yang menggunakan framework laravel sebagai beckend.

## Requirment
- [npm](https://www.npmjs.com/) => tidak wajib
- [composer](https://getcomposer.org/download/) => wajib

- webserver (apache2, ngix) => tidak wajib pada fase Development, karena bisa menggunakan servis bawaan laravel.
- PHP 5.6 ^ => wajib 
- mysql server (mysql 5.5 ^) => wajib
- Point 3 di atas bisa didapatkan menggunakan XAMPP (download terbaru)

## Package Installed

Dalam proyek ini sudah ada beberapa paket yang saya install untuk kebutuhan pengembangan sistem, diantarnya:

* CollectiveHtml
* Debugbar
* Faker Data
* Dompdf
> Setiap anggota tim yang menambahkan paket, harap edit file readme ini dan tambahkan dalam list.

## Installation
- kloning repositori ini dengan perintah `git clone https://loghoz@bitbucket.org/loghoz/laravel_irapot.git`. Anda juga bisa menggunakan SSH clone, namun tambahkan terlebih dahulu ssh key anda ke bitbucket.
- masuk ke direktori `cd laravel_irapot`
- lakukan instalasi paket larvel dengan perintah `composer install`
- https://drive.google.com/file/d/1-1CEFEduEhU1nbwWlZ4V-Y-Ft_WTjpcQ/view?usp=sharing Download File Public MariBaca dan extract didalam folder Public.
- duplikat dan ubah nama file .env.example ke .env. Linux atau Mac dapet mengikuti perintah ini `cp .env.example .env`
- `php artisan key:generate` untuk mendapatkan key app 
- `php artisan migrate:refresh --seed` untuk memigrasikan blueprint database dan isi database (setting database terlebih dahulu)

## Setting database

Buka file .env lalu isi sesuai dengan dengan role database anda

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=nama_database
    DB_USERNAME=root
    DB_PASSWORD=kosongkan_saja_kalo_pakai_xampp

## Run App

Terakhir jalankan perintah `php artisan serve` untuk menjalankan servis Laravel.
> jika proyek ini anda masukan dalam /var/www atau htdocs, anda tidak perlu menjalankan perintah di atas.