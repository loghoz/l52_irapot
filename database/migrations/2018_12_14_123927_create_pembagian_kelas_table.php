<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembagianKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembagian_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelas_id')->unsigned();
            $table->integer('siswa_id')->unsigned();
            $table->integer('tahun_ajaran_id')->unsigned();
            $table->timestamps();

            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onDelete('CASCADE');

            $table->foreign('siswa_id')
                ->references('id')
                ->on('siswas')
                ->onDelete('CASCADE');

            $table->foreign('tahun_ajaran_id')
                ->references('id')
                ->on('tahun_ajarans')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pembagian_kelas');
    }
}
