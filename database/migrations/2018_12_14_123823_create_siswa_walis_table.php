<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaWalisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa_walis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_induk');
            $table->string('ayah_nama');
            $table->string('ayah_tl');
            $table->string('ayah_pekerjaan');
            $table->string('ayah_penghasilan');
            $table->string('ayah_pendidikan');
            $table->string('ayah_telp');
            $table->string('ibu_nama');
            $table->string('ibu_tl');
            $table->string('ibu_pekerjaan');
            $table->string('ibu_penghasilan');
            $table->string('ibu_pendidikan');
            $table->string('ibu_telp');
            $table->string('ortu_alamat');
            $table->string('wali_nama');
            $table->string('wali_tl');
            $table->string('wali_pekerjaan');
            $table->string('wali_penghasilan');
            $table->string('wali_pendidikan');
            $table->string('wali_telp');
            $table->string('wali_alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswa_walis');
    }
}
