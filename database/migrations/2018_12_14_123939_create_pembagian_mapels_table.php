<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembagianMapelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembagian_mapels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jurusan_id')->unsigned();
            $table->integer('mata_pelajaran_id')->unsigned();
            $table->integer('guru_id')->unsigned();
            $table->integer('tahun_ajaran_id')->unsigned();
            $table->timestamps();

            $table->foreign('jurusan_id')
                ->references('id')
                ->on('jurusans')
                ->onDelete('CASCADE');

            $table->foreign('mata_pelajaran_id')
                ->references('id')
                ->on('mata_pelajarans')
                ->onDelete('CASCADE');

            $table->foreign('guru_id')
                ->references('id')
                ->on('gurus')
                ->onDelete('CASCADE');

            $table->foreign('tahun_ajaran_id')
                ->references('id')
                ->on('tahun_ajarans')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pembagian_mapels');
    }
}
