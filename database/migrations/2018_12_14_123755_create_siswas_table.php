<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agama_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->string('no_induk');
            $table->string('nama');
            $table->string('nisn');
            $table->string('jk');
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('anak_ke');
            $table->string('sekolah_asal');
            $table->string('kelas_terima');
            $table->string('tanggal_terima');
            $table->string('alamat');
            $table->string('rt');
            $table->string('rw');
            $table->string('desa');
            $table->string('kecamatan');
            $table->string('kabupaten');
            $table->string('provinsi');
            $table->string('kode_pos');
            $table->string('telp');
            $table->string('photo');
            $table->string('hapus');
            $table->timestamps();

            $table->foreign('agama_id')
                ->references('id')
                ->on('agamas')
                ->onDelete('CASCADE');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswas');
    }
}
