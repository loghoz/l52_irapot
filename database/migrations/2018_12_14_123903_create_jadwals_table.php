<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalsTable extends Migration
{

    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guru_id')->unsigned();
            $table->integer('mata_pelajaran_id')->unsigned();
            $table->integer('hari_id')->unsigned();
            $table->integer('tahun_ajaran_id')->unsigned();
            $table->string('kode_kelas');
            $table->string('jam_ajar');
            $table->string('jam_mulai');
            $table->string('jam_selesai');
            $table->timestamps();

            $table->foreign('guru_id')
                ->references('id')
                ->on('gurus')
                ->onDelete('CASCADE');

            $table->foreign('mata_pelajaran_id')
                ->references('id')
                ->on('mata_pelajarans')
                ->onDelete('CASCADE');

            $table->foreign('hari_id')
                ->references('id')
                ->on('haris')
                ->onDelete('CASCADE');

            $table->foreign('tahun_ajaran_id')
                ->references('id')
                ->on('tahun_ajarans')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jadwals');
    }
}
