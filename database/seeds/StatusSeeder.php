<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
            'status' => 'Anak Kandung'
        ]);
        
        Status::create([
            'status' => 'Anak Angkat'
        ]);
    }
}
