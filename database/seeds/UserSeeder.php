<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        //sample admin
        App\User::create([
            'no_induk' => '9999999999',
            'email' => 'admin@smkpelitapesawaran.sch.id',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('admin@smkpelitapesawaran.sch.id'),
            'role' => 'admin'
        ]);

        // sample guru
        for ($i=1; $i < 10; $i++) { 
            App\User::create([
                'no_induk' => '11111'.$i,
                'email' => '11111'.$i.'@smkpelitapesawaran.sch.id',
                'password' => bcrypt('1sampai9'),
                'api_token' => bcrypt('11111'.$i.'@smkpelitapesawaran.sch.id'),
                'role' => 'guru'
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            App\User::create([
                'no_induk' => '22222'.$i,
                'email' => '22222'.$i.'@smkpelitapesawaran.sch.id',
                'password' => bcrypt('1sampai9'),
                'api_token' => bcrypt('22222'.$i.'@smkpelitapesawaran.sch.id'),
                'role' => 'guru'
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            App\User::create([
                'no_induk' => '111'.$i,
                'email' => '111'.$i.'@smkpelitapesawaran.sch.id',
                'password' => bcrypt('1sampai9'),
                'api_token' => bcrypt('111'.$i.'@smkpelitapesawaran.sch.id'),
                'role' => 'siswa'
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            App\User::create([
                'no_induk' => '222'.$i,
                'email' => '222'.$i.'@smkpelitapesawaran.sch.id',
                'password' => bcrypt('1sampai9'),
                'api_token' => bcrypt('222'.$i.'@smkpelitapesawaran.sch.id'),
                'role' => 'siswa'
            ]);
        }
    }
}
