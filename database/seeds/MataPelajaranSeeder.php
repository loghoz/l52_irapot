<?php

use Illuminate\Database\Seeder;
use App\MataPelajaran;

class MataPelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10 ; $i++) { 
            MataPelajaran::create([
                'kode_mat_pel'  => 'TK0'.$i,
                'mata_pelajaran'=> 'Teknik'.$i,
                'deskripsi'     => 'Mata Pelajaran Teknik Komputer Jaringan'
            ]);
        }
    }
}
