<?php

use Illuminate\Database\Seeder;
use App\Guru;
use Faker\Factory as Faker;

class GuruSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        //default
        Guru::create([
            'no_induk'      => '999999',
            'nama'          => '-',
            'jabatan_id'    => '1',
            'agama_id'      => '1',
            'jk'            => 'L',
            'tempat_lahir'  => '-',
            'tanggal_lahir' => '1992-01-08',
            'alamat'        => 'sdsad',
            'rt'            => '1',
            'rw'            => '1',
            'desa'          => 'sdsad',
            'kecamatan'     => 'sdsad',
            'kabupaten'     => 'sdsad',
            'provinsi'      => 'sdsad',
            'kode_pos'      => '35372',
            'telp'          => '+6289631073926',
            'photo'         => 'avatar5.png',
            'hapus'         => '0',
        ]);

        for ($i=1; $i < 10; $i++) { 
            Guru::create([
                'no_induk'      => '11111'.$i,
                'nama'          => $faker->name($gender = 'male'),
                'jabatan_id'    => '1',
                'agama_id'      => '1',
                'jk'            => 'L',
                'telp'          => '6432423',
                'photo'         => 'avatar5.png',
                'tempat_lahir'  => '-',
                'tanggal_lahir' => '1992-01-08',
                'alamat'        => 'sdsad',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'sdsad',
                'kecamatan'     => 'sdsad',
                'kabupaten'     => 'sdsad',
                'provinsi'      => 'sdsad',
                'kode_pos'      => '35372',
                'telp'          => '+6289631073926',
                'hapus'         => '0',
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            Guru::create([
                'no_induk'      => '22222'.$i,
                'nama'          => $faker->name($gender = 'female'),
                'jabatan_id'    => '1',
                'agama_id'      => '1',
                'jk'            => 'P',
                'telp'          => '6432423',
                'photo'         => 'avatar2.png',
                'tempat_lahir'  => '-',
                'tanggal_lahir' => '1992-01-08',
                'alamat'        => 'sdsad',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'sdsad',
                'kecamatan'     => 'sdsad',
                'kabupaten'     => 'sdsad',
                'provinsi'      => 'sdsad',
                'kode_pos'      => '35372',
                'telp'          => '+6289631073926',
                'hapus'         => '0',
            ]);
        }
    }
}
