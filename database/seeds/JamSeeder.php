<?php

use Illuminate\Database\Seeder;
use App\Jam;

class JamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jam::create(['jam' => '07:15:00']);
        Jam::create(['jam' => '07:55:00']);
        Jam::create(['jam' => '08:00:00']);
        Jam::create(['jam' => '08:35:00']);
        Jam::create(['jam' => '08:45:00']);
        Jam::create(['jam' => '09:05:00']);
        Jam::create(['jam' => '09:30:00']);
        Jam::create(['jam' => '09:45:00']);
        Jam::create(['jam' => '10:00:00']);
        Jam::create(['jam' => '10:30:00']);
        Jam::create(['jam' => '10:40:00']);
        Jam::create(['jam' => '11:15:00']);
        Jam::create(['jam' => '11:20:00']);
        Jam::create(['jam' => '12:00:00']);
        Jam::create(['jam' => '12:30:00']);
        Jam::create(['jam' => '13:15:00']);
    }
}
