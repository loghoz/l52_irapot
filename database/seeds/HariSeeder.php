<?php

use Illuminate\Database\Seeder;
use App\Hari;

class HariSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hari::create([
            'hari' => 'Senin',
            'day' => 'Monday'
        ]);
        Hari::create([
            'hari' => 'Selasa',
            'day' => 'Tuesday'
        ]);
        Hari::create([
            'hari' => 'Rabu',
            'day' => 'Wednesday'
        ]);
        Hari::create([
            'hari' => 'Kamis',
            'day' => 'Thursday'
        ]);
        Hari::create([
            'hari' => 'Jumat',
            'day' => 'Friday'
        ]);
        Hari::create([
            'hari' => 'Sabtu',
            'day' => 'Saturday'
        ]);
        Hari::create([
            'hari' => 'Minggu',
            'day' => 'Sunday'
        ]);
    }
}
