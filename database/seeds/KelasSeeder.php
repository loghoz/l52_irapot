<?php

use Illuminate\Database\Seeder;
use App\Kelas;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //kelas 10
      Kelas::create(['kode_kelas' => '10-AP1','tingkat' => '10','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '10-AP2','tingkat' => '10','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '10-AK1','tingkat' => '10','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '10-AK2','tingkat' => '10','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '10-PMS1','tingkat' => '10','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '10-PMS2','tingkat' => '10','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '10-TKJ1','tingkat' => '10','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '10-TKJ2','tingkat' => '10','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '10-MM1','tingkat' => '10','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '10-MM2','tingkat' => '10','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '10-FM1','tingkat' => '10','jurusan_id'=> 7]);
      Kelas::create(['kode_kelas' => '10-FM2','tingkat' => '10','jurusan_id'=> 7]);

      //kelas 11
      Kelas::create(['kode_kelas' => '11-AP1','tingkat' => '11','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '11-AP2','tingkat' => '11','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '11-AK1','tingkat' => '11','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '11-AK2','tingkat' => '11','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '11-PMS1','tingkat' => '11','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '11-PMS2','tingkat' => '11','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '11-TKJ1','tingkat' => '11','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '11-TKJ2','tingkat' => '11','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '11-MM1','tingkat' => '11','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '11-MM2','tingkat' => '11','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '11-FM1','tingkat' => '11','jurusan_id'=> 7]);
      Kelas::create(['kode_kelas' => '11-FM2','tingkat' => '11','jurusan_id'=> 7]);

      //kelas 12
      Kelas::create(['kode_kelas' => '12-AP1','tingkat' => '12','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '12-AP2','tingkat' => '12','jurusan_id'=> 2]);
      Kelas::create(['kode_kelas' => '12-AK1','tingkat' => '12','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '12-AK2','tingkat' => '12','jurusan_id'=> 3]);
      Kelas::create(['kode_kelas' => '12-PMS1','tingkat' => '12','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '12-PMS2','tingkat' => '12','jurusan_id'=> 4]);
      Kelas::create(['kode_kelas' => '12-TKJ1','tingkat' => '12','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '12-TKJ2','tingkat' => '12','jurusan_id'=> 5]);
      Kelas::create(['kode_kelas' => '12-MM1','tingkat' => '12','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '12-MM2','tingkat' => '12','jurusan_id'=> 6]);
      Kelas::create(['kode_kelas' => '12-FM1','tingkat' => '12','jurusan_id'=> 7]);
      Kelas::create(['kode_kelas' => '12-FM2','tingkat' => '12','jurusan_id'=> 7]);
    }
}
