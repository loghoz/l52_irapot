<?php

use Illuminate\Database\Seeder;
use App\Jurusan;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurusan::create([
            'kode_jurusan' => '-',
            'jurusan' => '-',
            'bidang_keahlian'=>'-'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'AP',
            'jurusan' => 'Administrasi Perkantoran',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'AK',
            'jurusan' => 'Akuntansi',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'PMS',
            'jurusan' => 'Pemasaran',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'TKJ',
            'jurusan' => 'Teknik Komputer dan Jaringan',
            'bidang_keahlian'=>'Teknik Komputer dan Informatika'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'MM',
            'jurusan' => 'Multimedia',
            'bidang_keahlian'=>'Teknik Komputer dan Informatika'
        ]);
    
        Jurusan::create([
            'kode_jurusan' => 'FM',
            'jurusan' => 'Farmasi',
            'bidang_keahlian'=>'Kesehatan'
        ]);
    }
}
