<?php

use Illuminate\Database\Seeder;
use App\SiswaWali;
use Faker\Factory as Faker;

class SiswaWaliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for ($i=1; $i < 10; $i++) { 
            SiswaWali::create([
                'no_induk'          => '111'.$i,
                'ayah_nama'         => 'yeah',
                'ayah_tl'           => '1957',
                'ayah_pekerjaan'    => 'yeah',
                'ayah_pendidikan'   => 'yeah',
                'ayah_penghasilan'  => 'yeah',
                'ayah_telp'         => '089631073926',
                'ibu_nama'          => 'yeah',
                'ibu_tl'            => '1960',
                'ibu_pekerjaan'     => 'yeah',
                'ibu_pendidikan'    => 'yeah',
                'ibu_penghasilan'   => 'yeah',
                'ibu_telp'          => '089631073926',
                'ortu_alamat'       => 'yeah',
                'wali_nama'         => 'yeah',
                'wali_tl'           => '1963',
                'wali_pekerjaan'    => 'yeah',
                'wali_pendidikan'   => 'yeah',
                'wali_penghasilan'  => 'yeah',
                'wali_alamat'       => 'yeah',
                'wali_telp'         => '089631073926'
            ]);
            
        }
        for ($i=1; $i < 10; $i++) { 
            SiswaWali::create([
                'no_induk'          => '222'.$i,
                'ayah_nama'         => 'yeah',
                'ayah_tl'           => '1957',
                'ayah_pekerjaan'    => 'yeah',
                'ayah_pendidikan'   => 'yeah',
                'ayah_penghasilan'  => 'yeah',
                'ayah_telp'         => '089631073926',
                'ibu_nama'          => 'yeah',
                'ibu_tl'            => '1960',
                'ibu_pekerjaan'     => 'yeah',
                'ibu_pendidikan'    => 'yeah',
                'ibu_penghasilan'   => 'yeah',
                'ibu_telp'          => '089631073926',
                'ortu_alamat'       => 'yeah',
                'wali_nama'         => 'yeah',
                'wali_tl'           => '1963',
                'wali_pekerjaan'    => 'yeah',
                'wali_pendidikan'   => 'yeah',
                'wali_penghasilan'  => 'yeah',
                'wali_alamat'       => 'yeah',
                'wali_telp'         => '089631073926'
            ]);
            
        }
    }
}
