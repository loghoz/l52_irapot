<?php

use Illuminate\Database\Seeder;
use App\Penghasilan;

class PenghasilanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Penghasilan::create([
            'penghasilan' => '-'
        ]);
        Penghasilan::create([
            'penghasilan' => 'Kurang dari Rp. 500.000'
        ]);
        Penghasilan::create([
            'penghasilan' => 'Rp. 500.000 - Rp. 1.000.000'
        ]);
        Penghasilan::create([
            'penghasilan' => 'Rp. 1.000.000 - Rp. 2.000.000'
        ]);
        Penghasilan::create([
            'penghasilan' => 'Lebih dari Rp. 2.000.00'
        ]);
    }
}
