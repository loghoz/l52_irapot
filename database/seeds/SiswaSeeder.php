<?php

use Illuminate\Database\Seeder;
use App\Siswa;
use Faker\Factory as Faker;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=1; $i < 10; $i++) { 
            Siswa::create([
                'no_induk'      => '111'.$i,
                'nama'          => $faker->name($gender = 'male'),
                'status_id'     => '1',
                'agama_id'      => '1',
                'nisn'          => '1111'.$i,
                'jk'            => 'L',
                'tempat_lahir'  => 'Sukamarga',
                'tanggal_lahir' => '1992-01-08',
                'anak_ke'       => 'Sukamarga',
                'sekolah_asal'  => '1992-01-08',
                'kelas_terima'  => 'Sukamarga',
                'tanggal_terima'=> '1992-01-08',
                'alamat'        => 'Jl. Raya Penengahan Sukamarga',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'Sukamarga',
                'kecamatan'     => 'Gedongtataan',
                'kabupaten'     => 'Pesawaran',
                'provinsi'      => 'Lampung',
                'kode_pos'      => '35372',
                'telp'          =>'+6289631073926',
                'photo'         =>'avatar1.png',
                'hapus'         => '0',
            ]);
        }

        for ($i=1; $i < 10; $i++) { 
            Siswa::create([
                'no_induk'      => '222'.$i,
                'nama'          => $faker->name($gender = 'female'),
                'status_id'     => '1',
                'agama_id'      => '1',
                'nisn'          => '2222'.$i,
                'jk'            => 'P',
                'tempat_lahir'  => 'Sukamarga',
                'tanggal_lahir' => '1992-01-08',
                'anak_ke'       => 'Sukamarga',
                'sekolah_asal'  => '1992-01-08',
                'kelas_terima'  => 'Sukamarga',
                'tanggal_terima'=> '1992-01-08',
                'alamat'        => 'Jl. Raya Penengahan Sukamarga',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'Sukamarga',
                'kecamatan'     => 'Gedongtataan',
                'kabupaten'     => 'Pesawaran',
                'provinsi'      => 'Lampung',
                'kode_pos'      => '35372',
                'telp'          =>'+6289631073926',
                'photo'         =>'avatar3.png',
                'hapus'         => '0',
            ]);
        }
    }
}
