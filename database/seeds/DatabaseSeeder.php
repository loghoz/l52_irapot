<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        //primary data
        $this->call(AgamaSeeder::class);
        $this->call(HariSeeder::class);
        $this->call(JabatanSeeder::class);
        $this->call(JamSeeder::class);
        $this->call(PekerjaanSeeder::class);
        $this->call(PendidikanSeeder::class);
        $this->call(PenghasilanSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(TahunAjaranSeeder::class);
        $this->call(JurusanSeeder::class);
        $this->call(KelasSeeder::class);

        //optional
        $this->call(MataPelajaranSeeder::class);
        $this->call(GuruSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SiswaSeeder::class);
        $this->call(SiswaWaliSeeder::class);
    }
}
